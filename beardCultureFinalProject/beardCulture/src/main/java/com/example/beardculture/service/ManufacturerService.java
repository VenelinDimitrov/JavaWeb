package com.example.beardculture.service;

import com.example.beardculture.model.entity.Manufacturer;

public interface ManufacturerService {
    void addManufacturer(Manufacturer manufacturer);
}
