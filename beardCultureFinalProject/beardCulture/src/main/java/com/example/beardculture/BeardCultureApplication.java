package com.example.beardculture;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BeardCultureApplication {

    public static void main(String[] args) {
        SpringApplication.run(BeardCultureApplication.class, args);
    }

}
